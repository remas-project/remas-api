package com.ohapps.remasapi.model

data class MetricValue(val value: String)
