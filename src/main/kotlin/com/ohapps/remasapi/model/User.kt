package com.ohapps.remasapi.model

import com.ohapps.remasapi.annotation.NoArgConstructor

@NoArgConstructor
data class User(var id: String, var username: String)
