package com.ohapps.remasapi.enum

enum class UnitType {
    APARTMENT,
    SINGLE_FAMILY,
    CONDO
}
