package com.ohapps.remasapi.enum

enum class ExpenseType {
    YEARLY,
    MONTHLY,
    PERCENT_OF_RENT
}
