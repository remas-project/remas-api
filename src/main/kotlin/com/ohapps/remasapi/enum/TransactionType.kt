package com.ohapps.remasapi.enum

enum class TransactionType {
    INCOME,
    EXPENSE
}
