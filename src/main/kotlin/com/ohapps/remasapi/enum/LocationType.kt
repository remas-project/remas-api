package com.ohapps.remasapi.enum

enum class LocationType {
    CITY_STATE, ZIP, NEIGHBORHOOD, INTERSECTION
}
