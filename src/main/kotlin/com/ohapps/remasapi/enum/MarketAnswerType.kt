package com.ohapps.remasapi.enum

enum class MarketAnswerType {
    PERCENT,
    TRUE_FALSE,
    TEXT
}
