package com.ohapps.remasapi.enum

enum class MetricUnit {
    DOLLAR_AMOUNT,
    PERCENT,
    COUNT
}
