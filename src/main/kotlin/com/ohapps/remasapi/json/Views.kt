package com.ohapps.remasapi.json

class Views {
    open class Base
    class Read : Base()
    class Write : Base()
}
