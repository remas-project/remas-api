package com.ohapps.remasapi

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles

@SpringBootTest
@ActiveProfiles("test")
class RemasApiApplicationTests {

    @Test
    fun contextLoads() {
    }
}
